//NAVBAR


//déclaration des variables
let lastScrollTop = 0;
navbar = document.getElementById("navbar");

window.addEventListener("scroll", function() {
    //la variable scrollTop correspond à une mesure prise sur la page
  const scrollTop = window.pageTOffset ||
  this.document.documentElement.scrollTop;
//si le scrollTop est > à lastScrollTop (= si on est descendu)
  if (scrollTop > lastScrollTop) {
    //la navbar disparaît. Pour la faire disparaître, on agit sur son style et on la fait remonter de 50px donc elle disparaît de l'écran
    navbar.style.top="-50px";
  } else {
    // la navbar reste à sa place
    navbar.style.top = "0";
  }
  // on donne la valeur de lastScrollTop à ScrollTop pour le comparer à la prochaine fois qu'on scroll
  lastScrollTop = scrollTop;
});


//TYPED 
var typed = new Typed ('.typed', {
    strings: ['Bonjour et bienvenue sur mon CV en ligne!','J\'ai réalisé ce travail afin de mettre mes cours en application et de monter en compétences sur le front-end. J\'ai aimé personnaliser l\'apparence de ce CV! Cet exercice n\'a fait que renforcer mon choix de reconversion professionnelle dans le domaine de l\'informatique et plus particulièrement dans le monde du code'],
    typeSpeed: 15,
  });


//COMPTEUR LIVE
let compteur = 0;

$(window).scroll(function() {

  const top = $('.counter').offset().top - 
  window.innerHeight;

  if(compteur == 0 && $(window).scrollTop() > top) { // qd on arrive au début de l'élément et que le compteur est à 0 alors on lance le compteur
    $('.counter-value').each(function() {
      let $this = $(this),
        countTo = $this.attr('data-count');
      $({
        countNum: $this.text()
      }).animate({
        countNum : countTo
      },
      {
        duration: 5000, // (en ms donc = 5 secondes)
        easing:'swing',
        step: function (){
        $this.text(Math.floor(this.countNum)); // permet de compter chiffre après chiffre
        },
        complete: function(){
          $this.text(this.countNum);
        }
      });
    });
    compteur = 1;
  }
});
